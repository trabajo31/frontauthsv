import { Routes } from '@angular/router';

import { HomeComponent } from '../../pages/home/home.component';
import { UsersComponent } from '../../pages/users/users.component';
import { UserApiComponent } from '../../pages/user-api/user-api.component';
import { ClientsComponent } from '../../pages/clients/clients.component';
import { AplicationsComponent } from '../../pages/aplications/aplications.component';
import { MonitoringComponent } from '../../pages/monitoring/monitoring.component';
import { LogsChangeComponent } from '../../pages/logs-change/logs-change.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'home',           component: HomeComponent },
    { path: 'users',          component: UsersComponent },
    { path: 'users-api',      component: UserApiComponent },
    { path: 'clients',        component: ClientsComponent },
    { path: 'aplications',    component: AplicationsComponent },
    { path: 'monitoring',     component: MonitoringComponent },
    { path: 'logs-change',    component: LogsChangeComponent },
];
