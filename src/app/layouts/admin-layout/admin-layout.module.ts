import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { UsersComponent } from '../../pages/users/users.component';
import { UserApiComponent } from '../../pages/user-api/user-api.component';
import { ClientsComponent } from '../../pages/clients/clients.component';
import { AplicationsComponent } from '../../pages/aplications/aplications.component';
import { HomeComponent } from '../../pages/home/home.component';
import { MonitoringComponent } from '../../pages/monitoring/monitoring.component';
import { CreateEditUsersComponent } from '../../pages/users/modal/create-edit-users/create-edit-users.component';
import { CreateEditUsersApiComponent } from '../../pages/user-api/modal/create-edit-users-api/create-edit-users-api.component';
import { CreateEditClientsComponent } from '../../pages/clients/modal/create-edit-clients/create-edit-clients.component';
import { CreateAplicationsComponent } from '../../pages/aplications/modal/create-aplications/create-aplications.component';
import { EditAplicationsComponent } from '../../pages/aplications/modal/edit-aplications/edit-aplications.component';
import { ViewAplicationsComponent } from '../../pages/aplications/modal/view-aplications/view-aplications.component';
import { LogsChangeComponent } from '../../pages/logs-change/logs-change.component';

import { ComponentsModule } from 'app/components/components.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    DataTablesModule,
    MatTooltipModule,
    ComponentsModule,
  ],
  declarations: [
    HomeComponent,
    UsersComponent,
    UserApiComponent,
    ClientsComponent,
    AplicationsComponent,
    MonitoringComponent,
    LogsChangeComponent,
    CreateEditUsersComponent,
    CreateEditUsersApiComponent,
    CreateEditClientsComponent,
    CreateAplicationsComponent,
    EditAplicationsComponent,
    ViewAplicationsComponent
  ],
  entryComponents:[
    CreateEditUsersComponent,
    CreateEditClientsComponent,
    CreateAplicationsComponent,
    EditAplicationsComponent,
    ViewAplicationsComponent
  ],
})

export class AdminLayoutModule {}
