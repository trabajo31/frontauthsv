import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatablesService } from 'app/services/utils/datatables.service';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { CreateAplicationsComponent } from './modal/create-aplications/create-aplications.component';
import { EditAplicationsComponent } from './modal/edit-aplications/edit-aplications.component';
import { ViewAplicationsComponent } from './modal/view-aplications/view-aplications.component';
import { ClientsService } from 'app/services/clients.service';
import { AplicationsService } from 'app/services/aplications.service';
import { AlertService } from 'app/services/utils/alert.service';
import { UsersService } from 'app/services/users.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-aplications',
  templateUrl: './aplications.component.html'
})
export class AplicationsComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  loadingDt: boolean = false;
  user:any = [];
  clients:any;
  dtOptions: DataTables.Settings = {};
  
  dtTrigger: Subject<any> = new Subject();
  columsTable = [
    { data: "id" },
    { data: "client.name" },
    { data: "name" },
    { data: "ip" },
    { data: "url" },
    { render: function (data: any, type: any, full: any) {
        var checked = (full.state == 1) ? "checked" : "";        
        return `<button class="btn btn-primary btn-link btn-just-icon mat-raised-button mat-button-base editTbl" mat-raised-button>
                <span class="mat-button-wrapper"><i class="material-icons">mode_edit</i></span>
              </button>
              <div class="togglebutton">
                <label>
                  <input id="`+full.id+`" type="checkbox" `+checked+` class="activeTbl"><span class="toggle"></span>
                </label>
              </div>`;
      },
    },
    { render: function (data: any, type: any, full: any) {
          return `<button class="btn btn-primary btn-link btn-just-icon mat-raised-button mat-button-base viewTbl" mat-raised-button>
                  <span class="mat-button-wrapper"><i class="material-icons">visibility</i></span>
                </button>`;
        }
    }
  ];

  constructor(
    private datatablesService: DatatablesService,
    private alertService: AlertService,
    private notifyService: NotifyService,
    private clientsService: ClientsService,
    private aplicationsService: AplicationsService,
    public usersService: UsersService,
    public dialog: MatDialog) { 
    }

  ngOnInit(): void {
    this.user = this.usersService.getUser();
    this.buildDataTeble();
    this.getClients();
  }

  getClients(){
    this.clientsService.getClientList().subscribe(resp => {
      this.clients = resp.data;
    });
  }

  buildDataTeble() {
    const that = this;
    this.dtOptions = this.datatablesService.configServerSide('/aplications/all', this.columsTable);
    let columVisible = (this.user.admin == 1) ? [ 0, 6 ] : [ 0, 5 ];
    this.dtOptions.columnDefs = [
      { className: 'text-center', targets: [1,2,3,5,6] },
      { visible: false, targets: columVisible }
    ];

    this.dtOptions.rowCallback = function(row: Node, data: any[] | Object, index: number){
      $('td .editTbl', row).on('click', () => {
        that.editApp(data);
      });

      $('td .viewTbl', row).on('click', () => {
        that.viewApp(data);
      });
      
      $('td .activeTbl', row).on('change', () => {
        var dataAplication = { id: data['id'], state: null };
        if ($('.activeTbl#'+data['id']).is(':checked')){
          dataAplication.state = 1;
        }else{
          dataAplication.state = 0;
        }

        that.changeAppState(dataAplication, data['name']);
      });
    }
  }

  addApp(){
    const dialogRef = this.dialog.open(CreateAplicationsComponent, {
      data: { title: 'Agregar', clients: this.clients },
      width: '920px',
      disableClose: true
    });

    this.closeDialog(dialogRef);
  }

  editApp(dataApp:any){
    const dialogRef = this.dialog.open(EditAplicationsComponent, {
      data: { title: 'Editar', clients: this.clients, aplications: dataApp },
      width: '920px',
      disableClose: true
    });

    this.closeDialog(dialogRef);
  }

  viewApp(dataApp:any){
    const dialogRef = this.dialog.open(ViewAplicationsComponent, {
      data: { title: 'Consultar', aplications: dataApp },
      width: '920px',
      disableClose: true
    });

    this.closeDialog(dialogRef);
  }

  changeAppState(dataAplication:any, appName:string){
    this.alertService.confirm(
      "Cambiar estado de la aplicación " + appName, 
      "¿Está seguro de cambiar el estado de la aplicación, esto podría ponerla en mantenimiento?"
    ).then((res) => {
      if (res) {         
        this.loadingDt = true;
        this.aplicationsService.changeStateAplications(dataAplication).subscribe((response:any) => {
          this.loadingDt = false;
          if (response.status === 200) {
            this.notifyService.success('Estado de la aplicacion cambiado exitosamente');
            this.renderTable();
          } else if (response.status === 422) {
              var msgerror = '';
              $.each(response.errors, function (indexInArray, valueOfElement) {
                  msgerror += valueOfElement + '<br/>';
              });
              this.alertService.warning('Validacion', msgerror);
          } else {
              this.notifyService.error('Error cambiando el estado de la aplicacion');
          }
        })
      }else{
        if ($('.activeTbl#'+dataAplication.id).is(':checked')){
          $('.activeTbl#'+dataAplication.id).prop('checked', false);
        }else{
          $('.activeTbl#'+dataAplication.id).prop('checked', true);
        }
      }
    });
  }

  closeDialog(dialogRef) {
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'ok') {
        this.renderTable();
      }
    });
  }

  renderTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

}
