import { Component, OnInit, Inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CrytAESService } from 'app/services/utils/cryt-aes.service';
import { AplicationsService } from 'app/services/aplications.service';
import { AlertService } from 'app/services/utils/alert.service';
import { NotifyService } from 'app/services/utils/notify.service';

@Component({
  selector: 'app-create-aplications',
  templateUrl: './create-aplications.component.html'
})
export class CreateAplicationsComponent implements OnInit {
  public aplicationsForm: FormGroup;
  public loading: boolean = false;
  public title: string;
  public aplications = [];
  public clients = [];
  public formData = new FormData();
  public showPassword: boolean;
  public showPasswordRep: boolean;
  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
  selectedClient:any;

  constructor(private dialogRef: MatDialogRef<CreateAplicationsComponent>,
    private formBuild: FormBuilder,
    private alertService: AlertService,
    private notifyService: NotifyService,
    private aplicationsService: AplicationsService,
    private cryptService: CrytAESService,
    @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit(): void {
    this.title = this.data.title;
    this.clients = this.data.clients;
  
    this.buildForms();
  }

  buildForms() {
    this.aplicationsForm = this.formBuild.group({
        client: new FormControl('', Validators.required),
        name: new FormControl('', Validators.required),
        ip: new FormControl('', [Validators.required, Validators.pattern(this.ipPattern)]),
        url: new FormControl('', Validators.required),
        path: new FormControl('', Validators.required),
        ip_db: new FormControl('', [Validators.required, Validators.pattern(this.ipPattern)]),
        port_db: new FormControl('', Validators.required),
        user_db: new FormControl('', Validators.required),
        pass_db: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
        ip_db_rep: new FormControl('', [Validators.required, Validators.pattern(this.ipPattern)]),
        port_db_rep: new FormControl('', Validators.required),
        user_db_rep: new FormControl('', Validators.required),
        pass_db_rep: new FormControl('', Validators.required),
        description_rep: new FormControl('', Validators.required),
    });
  }

  save() {
    if (!this.aplicationsForm.invalid) {
      this.loading = true;
      const aplicationData = this.aplicationsForm.value;
      aplicationData.pass_db = this.cryptService.encrypt(aplicationData.pass_db);
      aplicationData.pass_db_rep = this.cryptService.encrypt(aplicationData.pass_db_rep);
      this.aplicationsService.addAplications(aplicationData).subscribe((resp) => {
        this.loading = false;
        if (resp.status === 200) {
          this.notifyService.success('Aplicacion agregada exitosamente');
          this.dialogRef.close('ok');
        } else if (resp.status === 422) {
            var msgerror = '';
            $.each(resp.errors, function (indexInArray, valueOfElement) {
                msgerror += valueOfElement + '<br/>';
            });
            this.alertService.warning('Validacion', msgerror);
        } else {
            this.notifyService.error('Error guardando la Aplicacion');
        }
      });
    }
  }

  get app(): { [key: string]: AbstractControl } {
    return this.aplicationsForm.controls;
  }

}
