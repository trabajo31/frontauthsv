import { Component, OnInit, Inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CrytAESService } from 'app/services/utils/cryt-aes.service';
import { AlertService } from 'app/services/utils/alert.service';
import { AplicationsService } from 'app/services/aplications.service';
import { NotifyService } from 'app/services/utils/notify.service';

@Component({
  selector: 'app-edit-aplications',
  templateUrl: './edit-aplications.component.html'
})
export class EditAplicationsComponent implements OnInit {
  public aplicationsForm: FormGroup;
  public dbPrincipalForm: FormGroup;
  public dbReplicForm: FormGroup;
  public loading: boolean = false;
  public title: string;
  public aplications:any;
  public clients = [];
  public formData = new FormData();
  dataEdit = false;
  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
  selectedClient:any;
  public showPassword: boolean;
  public showPasswordRep: boolean;
  
  constructor(private dialogRef: MatDialogRef<EditAplicationsComponent>,
    private formBuild: FormBuilder,
    private alertService: AlertService,
    private notifyService: NotifyService,
    private cryptService: CrytAESService,
    private aplicationsService: AplicationsService,
    @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit(): void {
    this.title = this.data.title;
    this.clients = this.data.clients;
    this.aplications = this.data.aplications;

    this.buildForms();
    this.setDataApp();
  }
  
  buildForms() {
    this.aplicationsForm = this.formBuild.group({
        id: new FormControl('', Validators.required),
        client: new FormControl('', Validators.required),
        name: new FormControl('', Validators.required),
        ip: new FormControl('', [Validators.required, Validators.pattern(this.ipPattern)]),
        url: new FormControl('', Validators.required),
        path: new FormControl('', Validators.required),
    });

    this.dbPrincipalForm = this.formBuild.group({
        id_db: new FormControl('', Validators.required),
        ip_db: new FormControl('', [Validators.required, Validators.pattern(this.ipPattern)]),
        port_db: new FormControl('', Validators.required),
        user_db: new FormControl('', Validators.required),
        pass_db: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
    });

    this.dbReplicForm = this.formBuild.group({
        id_db_rep: new FormControl('', Validators.required),
        ip_db_rep: new FormControl('', [Validators.required, Validators.pattern(this.ipPattern)]),
        port_db_rep: new FormControl('', Validators.required),
        user_db_rep: new FormControl('', Validators.required),
        pass_db_rep: new FormControl('', Validators.required),
        description_rep: new FormControl('', Validators.required),
    });
  }

  setDataApp(){
    this.selectedClient = '';
    this.selectedClient = this.aplications.client_id;
    this.aplicationsForm.controls['id'].setValue(this.aplications.id);
    this.aplicationsForm.controls['name'].setValue(this.aplications.name);
    this.aplicationsForm.controls['ip'].setValue(this.aplications.ip);
    this.aplicationsForm.controls['url'].setValue(this.aplications.url);
    this.aplicationsForm.controls['path'].setValue(this.aplications.path);
    
    var detalles = this.aplications.detalles;

    detalles.forEach(detalle => {
      detalle.pass_db = this.cryptService.decrypt(detalle.pass_db);
      if(detalle.types.id == 1){
        this.dbPrincipalForm.controls['id_db'].setValue(detalle.id);
        this.dbPrincipalForm.controls['ip_db'].setValue(detalle.ip_db);
        this.dbPrincipalForm.controls['port_db'].setValue(detalle.port_db);
        this.dbPrincipalForm.controls['user_db'].setValue(detalle.user_db);
        this.dbPrincipalForm.controls['pass_db'].setValue(detalle.pass_db);
        this.dbPrincipalForm.controls['description'].setValue(detalle.description);
      }else{
        this.dbReplicForm.controls['id_db_rep'].setValue(detalle.id);
        this.dbReplicForm.controls['ip_db_rep'].setValue(detalle.ip_db);
        this.dbReplicForm.controls['port_db_rep'].setValue(detalle.port_db);
        this.dbReplicForm.controls['user_db_rep'].setValue(detalle.user_db);
        this.dbReplicForm.controls['pass_db_rep'].setValue(detalle.pass_db);
        this.dbReplicForm.controls['description_rep'].setValue(detalle.description);
      }
    });
    
  }
  
  editApp(){
    if (!this.aplicationsForm.invalid) {
      this.loading = true;
      const aplicationData = this.aplicationsForm.value;
      this.aplicationsService.updateAplications(aplicationData).subscribe((resp) => {
        this.loading = false;
        if (resp.status === 200) {
          this.notifyService.success('Aplicacion editada exitosamente');
          this.dataEdit = true;
        } else if (resp.status === 422) {
            var msgerror = '';
            $.each(resp.errors, function (indexInArray, valueOfElement) {
                msgerror += valueOfElement + '<br/>';
            });
            this.alertService.warning('Validacion', msgerror);
        } else {
            this.notifyService.error('Error editando la Aplicacion');
        }
      });
    }
  }

  editDB(){
    if (!this.dbPrincipalForm.invalid) {
      this.loading = true;
      const dbData = this.dbPrincipalForm.value;
      dbData.pass_db = this.cryptService.encrypt(dbData.pass_db);
      this.aplicationsService.updateAplicationsDetails(dbData).subscribe((resp) => {
        this.loading = false;
        if (resp.status === 200) {
          this.notifyService.success('Base de datos principal editada exitosamente');
          this.dataEdit = true;
        } else if (resp.status === 422) {
            var msgerror = '';
            $.each(resp.errors, function (indexInArray, valueOfElement) {
                msgerror += valueOfElement + '<br/>';
            });
            this.alertService.warning('Validacion', msgerror);
        } else {
            this.notifyService.error('Error editando la base de datos principal');
        }
      });
    } 
  }

  editDBR(){
    if (!this.dbReplicForm.invalid) {
      this.loading = true;
      const dbData = this.dbReplicForm.value;
      dbData.pass_db_rep = this.cryptService.encrypt(dbData.pass_db_rep);
      this.aplicationsService.updateAplicationsDetails(dbData).subscribe((resp) => {
        this.loading = false;
        if (resp.status === 200) {
          this.notifyService.success('Base de datos replica editada exitosamente');
          this.dataEdit = true;
        } else if (resp.status === 422) {
            var msgerror = '';
            $.each(resp.errors, function (indexInArray, valueOfElement) {
                msgerror += valueOfElement + '<br/>';
            });
            this.alertService.warning('Validacion', msgerror);
        } else {
            this.notifyService.error('Error editando la base de datos replica');
        }
      });
    }
  }

  get app(): { [key: string]: AbstractControl } {
    return this.aplicationsForm.controls;
  }

  get dbP(): { [key: string]: AbstractControl } {
     return this.dbPrincipalForm.controls;
  }

  get dbR(): { [key: string]: AbstractControl } {
    return this.dbReplicForm.controls;
  }

}
