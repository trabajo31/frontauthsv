import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CrytAESService } from 'app/services/utils/cryt-aes.service';

@Component({
  selector: 'app-view-aplications',
  templateUrl: './view-aplications.component.html'
})
export class ViewAplicationsComponent implements OnInit {
  
  public aplicationsForm: FormGroup;
  public dbPrincipalForm: FormGroup;
  public dbReplicForm: FormGroup;
  public loading: boolean = false;
  public title: string;
  public aplications:any;
  public formData = new FormData();
  selectedClient:any;
  public showPassword: boolean;
  public showPasswordRep: boolean;
  
  constructor(private dialogRef: MatDialogRef<ViewAplicationsComponent>,
    private formBuild: FormBuilder,
    private cryptService: CrytAESService,
    @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit(): void {
    this.title = this.data.title;
    this.aplications = this.data.aplications;

    this.buildForms();
    this.setDataApp();
  }

  buildForms() {
    this.aplicationsForm = this.formBuild.group({
        id: new FormControl(''),
        client: new FormControl(''),
        name: new FormControl(''),
        ip: new FormControl(''),
        url: new FormControl(''),
        path: new FormControl(''),
    });

    this.dbPrincipalForm = this.formBuild.group({
        id_db: new FormControl(''),
        ip_db: new FormControl(''),
        port_db: new FormControl(''),
        user_db: new FormControl(''),
        description: new FormControl(''),
    });

    this.dbReplicForm = this.formBuild.group({
        id_db_rep: new FormControl(''),
        ip_db_rep: new FormControl(''),
        port_db_rep: new FormControl(''),
        user_db_rep: new FormControl(''),
        description_rep: new FormControl(''),
    });
  }

  setDataApp(){
    this.aplicationsForm.controls['id'].setValue(this.aplications.id);
    this.aplicationsForm.controls['client'].setValue(this.aplications.client.name);
    this.aplicationsForm.controls['name'].setValue(this.aplications.name);
    this.aplicationsForm.controls['ip'].setValue(this.aplications.ip);
    this.aplicationsForm.controls['url'].setValue(this.aplications.url);
    this.aplicationsForm.controls['path'].setValue(this.aplications.path);
    
    var detalles = this.aplications.detalles;

    detalles.forEach(detalle => {
      if(detalle.types.id == 1){
        this.dbPrincipalForm.controls['id_db'].setValue(detalle.id);
        this.dbPrincipalForm.controls['ip_db'].setValue(detalle.ip_db);
        this.dbPrincipalForm.controls['port_db'].setValue(detalle.port_db);
        this.dbPrincipalForm.controls['user_db'].setValue(detalle.user_db);
        this.dbPrincipalForm.controls['description'].setValue(detalle.description);
      }else{
        this.dbReplicForm.controls['id_db_rep'].setValue(detalle.id);
        this.dbReplicForm.controls['ip_db_rep'].setValue(detalle.ip_db);
        this.dbReplicForm.controls['port_db_rep'].setValue(detalle.port_db);
        this.dbReplicForm.controls['user_db_rep'].setValue(detalle.user_db);
        this.dbReplicForm.controls['description_rep'].setValue(detalle.description);
      }
    });
    
  }

}
