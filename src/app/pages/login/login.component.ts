import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from 'app/services/login.service';
import { TokenService } from 'app/services/token.service';
import { NotifyService } from 'app/services/utils/notify.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  user = { user: '', password: '' }
  public loginForm: FormGroup;
  public preloader: boolean;
  public data = new FormData();

  private token = sessionStorage.getItem('token');

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private tokenService: TokenService,
    private notifyService: NotifyService,
    private _router: Router) { }

  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.loginForm = this.formBuilder.group({
      user: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  onLogin() {
    this.preloader = true;
    if(this.loginForm.valid){
      this.data.append('user_network', this.loginForm.controls.user.value);
      this.data.append('password', this.loginForm.controls.password.value);
      this.loginService.login(this.data).subscribe(resp => {
        // console.log(resp.data);
        if (resp.codigo == 200) {
          this.tokenService.setToken(resp.data.token);
          this._router.navigate(['/home']);
        } else {
          this.notifyService.warning(resp.message);
          this.preloader = false;
        }
      });
    }else{
      this.notifyService.warning('Ingrese su usuario y contraseña');
      this.preloader = false;
    }
  }

}
