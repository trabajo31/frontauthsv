import { Component, OnInit , Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-create-sv',
  templateUrl: './create-sv.component.html',
  styleUrls: ['./create-sv.component.scss']
})
export class CreateSvComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<CreateSvComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
  ) { 
    // console.log(this.data);
  }

  ngOnInit(): void {
    
  }

  save(): void {      
    this.dialogRef.close({ 'ok' : 'luis', 'message': 'ok'});
  }

  cancelar(): void {
    this.dialogRef.close();
  }

}
