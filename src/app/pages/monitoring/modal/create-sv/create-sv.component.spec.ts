import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSvComponent } from './create-sv.component';

describe('CreateSvComponent', () => {
  let component: CreateSvComponent;
  let fixture: ComponentFixture<CreateSvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
