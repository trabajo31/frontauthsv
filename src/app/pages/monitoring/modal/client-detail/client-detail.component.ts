import { Component, OnInit , Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import { AplicationsService } from 'app/services/aplications.service';
import { DataVirtualService } from 'app/services/utils/models/DataVirtualService';
import { Client } from 'app/services/utils/models/Client';
import { UserEdit } from 'app/services/utils/models/UserEdit';
import { UserCreate } from 'app/services/utils/models/UserCreate';

@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.scss']
})
export class ClientDetailComponent implements OnInit {
  loadingDt: boolean = false;
  virtualService: DataVirtualService = new DataVirtualService();
  ClientService: Client = Client;
  UserCreate: UserCreate = UserCreate;
  UserEdit: UserEdit = UserEdit;
  result : any = { 'ok' : 'luis', 'message': 'ok'};
  panelOpenState = false;

  constructor(
    private aplicationsService: AplicationsService,
    private dialogRef: MatDialogRef<ClientDetailComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
  ) { }

  ngOnInit(): void {
  }

  getAplicationById(id_service){
    this.loadingDt = true;
    this.aplicationsService.getServiceById(id_service).subscribe((resp) => {
      this.loadingDt = false;
      this.virtualService = resp;
      
      this.ClientService = resp.client;
      this.UserCreate = resp.user_create;
      this.UserEdit = resp.user_edit;
      // this.dtTrigger.next();
    });
  }

  save(): void {      
    this.dialogRef.close(this.result);
  }

  cancelar(): void {
    this.dialogRef.close();
  }
}
