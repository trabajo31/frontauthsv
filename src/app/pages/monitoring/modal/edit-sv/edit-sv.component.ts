import { Component, OnInit , Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import { AplicationsService } from 'app/services/aplications.service';
import { ClientsService } from 'app/services/clients.service';
import { DataVirtualService } from 'app/services/utils/models/DataVirtualService';
import { Client } from 'app/services/utils/models/Client';
import { UserEdit } from 'app/services/utils/models/UserEdit';
import { UserCreate } from 'app/services/utils/models/UserCreate';
interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-edit-sv',
  templateUrl: './edit-sv.component.html',
  styleUrls: ['./edit-sv.component.scss']
})
export class EditSvComponent implements OnInit {
  
  email = new FormControl('', [Validators.required]);
  loadingDt: boolean = false;
  virtualService: DataVirtualService = new DataVirtualService();
  ClientService: Client = Client;
  AllClients: Client;
  UserCreate: UserCreate = UserCreate;
  UserEdit: UserEdit = UserEdit;
  result : any = { 'ok' : 'luis', 'message': 'ok'};
  panelOpenState = false;
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'},
  ];

  constructor(
    private aplicationsService: AplicationsService,
    private clientsService: ClientsService,
    private dialogRef: MatDialogRef<EditSvComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
  ) { }

  ngOnInit(): void {
    // console.log(this.data.id);
    this.getAplicationById(this.data.id);
    this.getAllClient();
    let vat = 32;
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  getAplicationById(id_service){
    this.loadingDt = true;
    this.aplicationsService.getServiceById(id_service).subscribe((resp) => {
      this.loadingDt = false;
      this.virtualService = resp;
      
      this.ClientService = resp.client;
      this.UserCreate = resp.user_create;
      this.UserEdit = resp.user_edit;
      // this.dtTrigger.next();
    });
  }

  getAllClient() {
    this.loadingDt = true;
    this.clientsService.getClientAll().subscribe((resp) => {
      this.loadingDt = false;
      this.AllClients = resp.data;
      console.log(this.AllClients);
      // this.dtTrigger.next();
    });
  }

  save(): void {      
    this.dialogRef.close(this.result);
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  /**
   * @date 21-12-2021
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @description Funcion para abrir el modal de crear gestión.
   * @param {Number} id Contiene el id de la solicitud a editar
   * @param {Number} category Contiene el id de la categoria de la solicitud
   * @param {Number} status Contiene el id del estado de la solicitud
   * @param {String} description Contiene la descripcion de la solicitud
   */
        editVirtualService(updateClient) {
          this.clientsService.updateClient(updateClient).subscribe((resp) => {
            // this.loadingDt = false;
            // this.AllClients = resp.data;
            // console.log(this.AllClients);
            // this.dtTrigger.next();
          });
        // const dialogRef = this.dialog.open(EditSvComponent, {
        //     data: {
        //         id,
        //         // category,
        //         // status,
        //         // description,
        //         // panelTypeDefault: this.panelType[0],
        //         // categoryDefault: this.categoryCtrl.value
        //     },
        //     disableClose: false,
        //     width: '800px'
        // });

        // dialogRef.afterClosed().subscribe(
        //     (result) => {
        //         if (result !== undefined) {
                  
        //             // this.panelTypeCtrl.setValue(result.panelTypeDefault);
        //             // this.categoryCtrl.setValue(result.categoryDefault);
        //             // this.ngOnInit();
        //         }
        //     }
        // );
    }

}
