import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualServicesDetailComponent } from './virtual-services-detail.component';

describe('VirtualServicesDetailComponent', () => {
  let component: VirtualServicesDetailComponent;
  let fixture: ComponentFixture<VirtualServicesDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VirtualServicesDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualServicesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
