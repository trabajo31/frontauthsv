import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatablesService } from 'app/services/utils/datatables.service';
import { DataTableDirective } from 'angular-datatables';
import { AplicationsService } from 'app/services/aplications.service';
import { AlertService } from 'app/services/utils/alert.service';
import { UsersService } from 'app/services/users.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog'
import { EditSvComponent } from './modal/edit-sv/edit-sv.component'
import { VirtualServicesDetailComponent } from './modal/virtual-services-detail/virtual-services-detail.component';

@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html'
})
export class MonitoringComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  loadingDt: boolean = false;
  user:any = [];
  aplications = [];
  aplicationsIni = [];
  
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  constructor(private aplicationsService: AplicationsService,
    private alertService: AlertService,
    private notifyService: NotifyService,
    private datatablesService: DatatablesService,
    private usersService: UsersService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.user = this.usersService.getUser();

    this.getAplications();
    this.buildDataTeble();
  }

  buildDataTeble() {
    this.dtOptions = this.datatablesService.config();
  }

  getAplications(){
    this.loadingDt = true;
    this.aplicationsService.getAplicationsAll().subscribe((resp) => {
      this.loadingDt = false;
      this.aplications = resp.data;
      console.log(resp);
      this.aplicationsIni = resp.data;
      this.dtTrigger.next();
    });
  }

  selectChange(app, selected){    
    const appSelected = app.detalles.filter(element => element.id === selected.value );

    this.alertService.confirm(
      "Cambiar conexión de " + app.name, 
      "¿Está seguro de cambiar la conexión de la aplicación " + app.name + " a la base de datos " + appSelected[0].types.name + "?"
    ).then((res) => {
      if (res) { 
        const changeApp = {
          id_app: app.id,
          db_active: selected.value,
          db_inactive: app.active,
        };
        
        this.loadingDt = true;
        this.aplicationsService.changeDbAplications(changeApp).subscribe((response:any) => {
          this.loadingDt = false;
          if (response.status === 200) {
            this.notifyService.success('Base de datos cambiada exitosamente');
          } else if (response.status === 422) {
              var msgerror = '';
              $.each(response.errors, function (indexInArray, valueOfElement) {
                  msgerror += valueOfElement + '<br/>';
              });
              this.alertService.warning('Validacion', msgerror);
          } else {
              this.notifyService.error('Error cambiando la base de datos');
          }
        })
      }else{
        this.aplications = [];
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
        });
        
        this.renderTable();
      }
    });

  }

  getValueView(detalles:any, active:any){
    let value = '';
    detalles.forEach(detalle=> {
      if(detalle.id == active){
        value =  detalle.types.name +' - '+ detalle.ip_db +':'+ detalle.port_db;
      }

    });
    return value;
  }

  renderTable(): void {
    setTimeout(() => {
      this.aplications = this.aplicationsIni;
      this.dtTrigger.next();
    },10)
  }

      /**
     * @date 21-12-2021
     * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
     * @description Funcion para abrir el modal de crear gestión.
     * @param {Number} id Contiene el id de la solicitud a editar
     * @param {Number} category Contiene el id de la categoria de la solicitud
     * @param {Number} status Contiene el id del estado de la solicitud
     * @param {String} description Contiene la descripcion de la solicitud
     */
       detailVirtualService(id) {
        const dialogRef = this.dialog.open(VirtualServicesDetailComponent, {
            data: {
                id,
                // category,
                // status,
                // description,
                // panelTypeDefault: this.panelType[0],
                // categoryDefault: this.categoryCtrl.value
            },
            disableClose: false,
            width: '800px'
        });

        dialogRef.afterClosed().subscribe(
            (result) => {
                if (result !== undefined) {
                  
                    // this.panelTypeCtrl.setValue(result.panelTypeDefault);
                    // this.categoryCtrl.setValue(result.categoryDefault);
                    // this.ngOnInit();
                }
            }
        );
    }

      /**
     * @date 21-12-2021
     * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
     * @description Funcion para abrir el modal de crear gestión.
     * @param {Number} id Contiene el id de la solicitud a editar
     * @param {Number} category Contiene el id de la categoria de la solicitud
     * @param {Number} status Contiene el id del estado de la solicitud
     * @param {String} description Contiene la descripcion de la solicitud
     */
      editVirtualService(id) {
        const dialogRef = this.dialog.open(EditSvComponent, {
            data: {
                id,
                // category,
                // status,
                // description,
                // panelTypeDefault: this.panelType[0],
                // categoryDefault: this.categoryCtrl.value
            },
            disableClose: false,
            width: '800px'
        });

        dialogRef.afterClosed().subscribe(
            (result) => {
                if (result !== undefined) {
                  
                    // this.panelTypeCtrl.setValue(result.panelTypeDefault);
                    // this.categoryCtrl.setValue(result.categoryDefault);
                    // this.ngOnInit();
                }
            }
        );
    }
}
