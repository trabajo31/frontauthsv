import { Component, OnInit, ViewChild  } from '@angular/core';
import { DatatablesService } from 'app/services/utils/datatables.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-logs-change',
  templateUrl: './logs-change.component.html'
})
export class LogsChangeComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  loadingDt: boolean = false;

  users:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  columsTable = [
    { data: "date_register" },
    { data: "user.user" },
    { data: "aplication.client.name" },
    { data: "aplication.name" },
    { data: "action" },
  ];

  constructor(private datatablesService: DatatablesService) { }

  ngOnInit(): void {
    this.buildDataTeble();
  }

  buildDataTeble() {
    this.dtOptions = this.datatablesService.configServerSide('/logs/all', this.columsTable, 20);

    this.dtOptions.columnDefs = [
      { className: 'text-center', targets: [0,2,3] },
    ];

    this.dtOptions.order = [[0, 'desc']];
  }

}
