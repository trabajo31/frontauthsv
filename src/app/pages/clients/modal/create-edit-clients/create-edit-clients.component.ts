import { Component, OnInit, Inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ClientsService } from 'app/services/clients.service';
import { AlertService } from 'app/services/utils/alert.service';
import { NotifyService } from 'app/services/utils/notify.service';

@Component({
  selector: 'app-create-edit-clients',
  templateUrl: './create-edit-clients.component.html'
})
export class CreateEditClientsComponent implements OnInit {
  public clientsForm: FormGroup;
  public loading: boolean = false;
  public title: string;
  public users = [];
  public formData = new FormData();
  selectedUsers = [];

  constructor(
    private dialogRef: MatDialogRef<CreateEditClientsComponent>,
    private formBuild: FormBuilder,
    private clientsService: ClientsService,
    private alertService: AlertService,
    private notifyService: NotifyService,
    @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit(): void {
    this.title = this.data.title;
    this.users = this.data.users;
    this.buildForm();
  }
  
  buildForm() {
    this.clientsForm = this.formBuild.group({
        name: new FormControl('', Validators.required),
        users: new FormControl('', Validators.required)
    });

    this.setDataClient();
  }

  setDataClient(){
    this.selectedUsers = [];
    if(this.data.client !== undefined) {
      var user = this.data.client.users;
      this.clientsForm.controls['name'].setValue(this.data.client.name);
      user.forEach(element => {
        this.selectedUsers.push(element.pivot.user_id);
      });
    }
  }

  save(){
    if (!this.clientsForm.invalid) {
      this.loading = true;
      if(this.data.client !== undefined) {         
        this.editClient();
      } else{
        this.addClient();
      }
    }
  }

  addClient() {
    const clientData = this.clientsForm.value;

    this.clientsService.addClient(clientData).subscribe((resp) => {
      this.loading = false;
      if (resp.status === 200) {
        this.notifyService.success('Cliente agregado exitosamente');
        this.dialogRef.close('ok');
      } else if (resp.status === 422) {
          var msgerror = '';
          $.each(resp.errors, function (indexInArray, valueOfElement) {
              msgerror += valueOfElement + '<br/>';
          });
          this.alertService.warning('Validacion', msgerror);
      } else {
          this.notifyService.error('Error guardando el cliente');
      }
    });
  }

  editClient() {
    const clientData = {
      ...this.clientsForm.value,
      id: this.data.client.id
    };
    
    this.clientsService.updateClient(clientData).subscribe((resp) => {
      this.loading = false;
      if (resp.status === 200) {
        this.notifyService.success('Cliente editado exitosamente');
        this.dialogRef.close('ok');
      } else if (resp.status === 422) {
          var msgerror = '';
          $.each(resp.errors, function (indexInArray, valueOfElement) {
              msgerror += valueOfElement + '<br/>';
          });
          this.alertService.warning('Validacion', msgerror);
      } else {
          this.notifyService.error('Error editando el cliente');
      }
    });
  }

  get validation(): { [key: string]: AbstractControl } {
    return this.clientsForm.controls;
  }

}
