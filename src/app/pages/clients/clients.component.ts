import { Component, OnInit, ViewChild  } from '@angular/core';
import { DatatablesService } from 'app/services/utils/datatables.service';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { CreateEditClientsComponent } from './modal/create-edit-clients/create-edit-clients.component';
import { UsersService } from 'app/services/users.service';
import { ClientsService } from 'app/services/clients.service';
import { AlertService } from 'app/services/utils/alert.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html'
})
export class ClientsComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  loadingDt: boolean = false;

  users:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  columsTable = [
    { data: "id" },
    { data: "name" },
    { render: function (data: any, type: any, full: any) {
        return `<button class="btn btn-primary btn-link btn-just-icon mat-raised-button mat-button-base editTbl" mat-raised-button>
                <span class="mat-button-wrapper"><i class="material-icons">mode_edit</i></span>
              </button>
              <button class="btn btn-danger btn-link btn-just-icon mat-raised-button mat-button-base deleteTbl" mat-raised-button>
                <span class="mat-button-wrapper"><i class="material-icons">delete</i></span>
              </button>`;
      } 
    },
  ];

  constructor(
    private datatablesService: DatatablesService,
    private usersService: UsersService,
    private alertService: AlertService,
    private notifyService: NotifyService,
    private clientsService: ClientsService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.buildDataTeble();
    this.getUsers();
  }

  getUsers(){
    this.usersService.getUserAllList().subscribe(resp => {
      this.users = resp.data;
    });
  }

  buildDataTeble() {
    const that = this;
    this.dtOptions = this.datatablesService.configServerSide('/clients/all', this.columsTable);

    this.dtOptions.columnDefs = [
      { className: 'text-center', targets: [0,2] },
      { width: "10%", targets: [2] },
      { visible: false, targets: [ 0 ] }
    ];

    this.dtOptions.rowCallback = function(row: Node, data: any[] | Object, index: number){
      $('td .editTbl', row).on('click', () => {
        that.editClient(data);
      });

      $('td .deleteTbl', row).on('click', () => {
        that.deleteClient(data);
      });
    }
  }

  addClient(){
    const dialogRef = this.dialog.open(CreateEditClientsComponent, {
      data: { title: 'Agregar', users: this.users },
      width: '420px',
      disableClose: true
    });

    this.closeDialog(dialogRef);
  }

  editClient(dataClient:any){
    const dialogRef = this.dialog.open(CreateEditClientsComponent, {
      data: { title: 'Editar', client: dataClient, users: this.users },
      width: '420px',
      disableClose: true
    });
    
    this.closeDialog(dialogRef);
  }

  deleteClient(dataClient:any){
    this.alertService.confirm(
      "Eliminar cliente", 
      "¿Esta seguro que desea eliminar el cliente "+dataClient.name+" ?"
    ).then((res) => {
      if (res) { 
        this.loadingDt = true;
        this.clientsService.deleteClient({'id' : dataClient.id}).subscribe((resp) => {
          if (resp.status === 200) {
            this.loadingDt = false;
            this.notifyService.success('Cliente eliminado exitosamente');
            this.renderTable();
          } else if (resp.status === 422) {
              var msgerror = '';
              $.each(resp.errors, function (indexInArray, valueOfElement) {
                  msgerror += valueOfElement + '<br/>';
              });
              this.alertService.warning('Validacion', msgerror);
          } else {
              this.notifyService.error('Error eliminando al cliente');
          }
        });
      }
    });
  }

  closeDialog(dialogRef) {
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'ok') {
        this.renderTable();
      }
    });
  }

  renderTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

}
