import { Component, OnInit, Inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AlertService } from 'app/services/utils/alert.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { UsersService } from 'app/services/users.service';

@Component({
  selector: 'app-create-edit-users',
  templateUrl: './create-edit-users.component.html'
})

export class CreateEditUsersComponent implements OnInit {
  public userForm: FormGroup;
  public loading: boolean = false;
  public title: string;
  public roles = [];
  public formData = new FormData();
  selectedRole:any;

  constructor( private dialogRef: MatDialogRef<CreateEditUsersComponent>,
    private formBuild: FormBuilder,
    private alertService: AlertService,
    private notifyService: NotifyService,
    private usersService: UsersService,
    @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit(): void {
    this.title = this.data.title;
    this.roles = this.data.roles;
    this.buildForm();
  }

  buildForm() {
    this.userForm = this.formBuild.group({
        id: new FormControl(''),
        user: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z.ñÑáéíóúÁÉÍÓÚ]+'), Validators.minLength(6), Validators.maxLength(30)]),
        name: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Zñ ÑáéíóúÁÉÍÓÚ]*$'), Validators.minLength(6), Validators.maxLength(60)]),
        type: new FormControl('', Validators.required)
    });

    this.setDataUser();
  }

  setDataUser(){
    this.selectedRole = '';
    if(this.data.user !== undefined) {
      this.selectedRole = this.data.user.admin;
      this.userForm.controls['id'].setValue(this.data.user.id);
      this.userForm.controls['user'].setValue(this.data.user.user);
      this.userForm.controls['name'].setValue(this.data.user.name);
      this.userForm.controls['type'].setValue(this.data.user.admin);
    }
  }
  
  save(){
    if (!this.userForm.invalid) {
      this.loading = true;
      if(this.data.user !== undefined) {         
        this.editUser();
      } else{
        this.addUser();
      }
    }
  }

  addUser() {
    const userData = this.userForm.value;

    this.usersService.addUser(userData).subscribe((resp) => {
      this.loading = false;
      if (resp.status === 200) {
        this.notifyService.success('Usuario agregado exitosamente');
        this.dialogRef.close('ok');
      } else if (resp.status === 422) {
          var msgerror = '';
          $.each(resp.errors, function (indexInArray, valueOfElement) {
              msgerror += valueOfElement + '<br/>';
          });
          this.alertService.warning('Validacion', msgerror);
      } else {
          this.notifyService.error('Error guardando el usuario');
      }
    });
  }

  editUser() {
    const userData = {
      ...this.userForm.value,
      id: this.data.user.id
    };
    
    this.usersService.updateUser(userData).subscribe((resp) => {
      this.loading = false;
      if (resp.status === 200) {
        this.notifyService.success('Usuario editado exitosamente');
        this.dialogRef.close('ok');
      } else if (resp.status === 422) {
          var msgerror = '';
          $.each(resp.errors, function (indexInArray, valueOfElement) {
              msgerror += valueOfElement + '<br/>';
          });
          this.alertService.warning('Validacion', msgerror);
      } else {
          this.notifyService.error('Error editando el usuario');
      }
    });
  }

  get validation(): { [key: string]: AbstractControl } {
    return this.userForm.controls;
  }

}
