import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatablesService } from 'app/services/utils/datatables.service';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { CreateEditUsersComponent } from './modal/create-edit-users/create-edit-users.component';
import { AlertService } from 'app/services/utils/alert.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { UsersService } from 'app/services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  loadingDt: boolean = false;

  roles:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  columsTable = [
    { data: "id" },
    { data: "user" },
    { data: "name" },
    { data: "role.name" },
    { render: function (data: any, type: any, full: any) {
        return `<button class="btn btn-primary btn-link btn-just-icon mat-raised-button mat-button-base editTbl" mat-raised-button>
            <span class="mat-button-wrapper"><i class="material-icons">mode_edit</i></span>
          </button>
          <button class="btn btn-danger btn-link btn-just-icon mat-raised-button mat-button-base deleteTbl" mat-raised-button>
            <span class="mat-button-wrapper"><i class="material-icons">delete</i></span>
          </button>`;
      } 
    },
  ];

  constructor(private datatablesService: DatatablesService,
    private usersService: UsersService,
    private alertService: AlertService,
    private notifyService: NotifyService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getRoles();
    this.buildDataTeble();
  }

  buildDataTeble() {
    const that = this;
    this.dtOptions = this.datatablesService.configServerSide('/users/all', this.columsTable);

    this.dtOptions.columnDefs = [
      { className: 'text-center', targets: [3,4] },
      { visible: false, targets: [ 0 ] }
    ];

    this.dtOptions.rowCallback = function(row: Node, data: any[] | Object, index: number){
      $('td .editTbl', row).on('click', () => {
        that.editUser(data);
      });

      $('td .deleteTbl', row).on('click', () => {
        that.deleteUser(data);
      });
    }
  }

  getRoles(){
    this.usersService.getRoles().subscribe(resp => {
      this.roles = resp.data;
    });
  }

  addUser(){
    const dialogRef = this.dialog.open(CreateEditUsersComponent, {
      data: { title: 'Agregar', roles: this.roles },
      width: '360px',
      disableClose: true
    });

    this.closeDialog(dialogRef);
  }

  editUser(dataUser:any){
    const dialogRef = this.dialog.open(CreateEditUsersComponent, {
      data: { title: 'Editar', user: dataUser, roles: this.roles },
      width: '360px',
      disableClose: true
    });
    
    this.closeDialog(dialogRef);
  }

  deleteUser(dataUser:any) {
    this.alertService.confirm(
      "Eliminacion de Usuario", 
      "¿Realmente desea eliminuar el usuario " + dataUser.user + " permanentemente ?"
    ).then((res) => {
      if (res) { 
        this.loadingDt = true;
        this.usersService.deleteUser({id : dataUser.id}).subscribe((resp) => {
          this.loadingDt = false;
          if (resp.status === 200) {
            this.notifyService.success('Usuario eliminado exitosamente');
            this.renderTable();
          } else if (resp.status === 422) {
              var msgerror = '';
              $.each(resp.errors, function (indexInArray, valueOfElement) {
                  msgerror += valueOfElement + '<br/>';
              });
              this.alertService.warning('Validacion', msgerror);
          } else {
              this.notifyService.error('Error eliminando al usuario');
          }
        });
      }
    });
  }

  closeDialog(dialogRef) {
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'ok') {
        this.renderTable();
      }
    });
  }

  renderTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

}
