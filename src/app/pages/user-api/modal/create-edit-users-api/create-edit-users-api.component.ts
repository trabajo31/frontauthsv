import { Component, OnInit, Inject } from '@angular/core';
import { AbstractControl, FormBuilder, NgForm, FormGroupDirective, FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AlertService } from 'app/services/utils/alert.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { UsersApiService } from 'app/services/users-api.service';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-create-edit-users-api',
  templateUrl: './create-edit-users-api.component.html'
})
export class CreateEditUsersApiComponent implements OnInit {
  public userApiForm: FormGroup;
  public loading: boolean = false;
  public title: string;
  public showPassword: boolean;
  public showConfirmPassword: boolean;
  public formData = new FormData();
  matcher = new MyErrorStateMatcher();

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
  passPattern = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}';
  selectedRole:any;

  constructor( private dialogRef: MatDialogRef<CreateEditUsersApiComponent>,
    private formBuild: FormBuilder,
    private alertService: AlertService,
    private notifyService: NotifyService,
    private usersApiService: UsersApiService,
    @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit(): void {
    this.title = this.data.title;
    this.buildForm();
  }

  buildForm() {
    this.userApiForm = this.formBuild.group({
        id: new FormControl(''),
        username: new FormControl('', [Validators.required, Validators.pattern(this.ipPattern)]),
        password: new FormControl('', [Validators.required, Validators.pattern(this.passPattern), Validators.minLength(8), Validators.maxLength(30)]),
        password_confirmation : new FormControl(''),
    }, { validator: this.checkPasswords });

    this.setDataUser();
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.password_confirmation.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  setDataUser(){
    this.selectedRole = '';
    if(this.data.user !== undefined) {
      this.selectedRole = this.data.user.admin;
      this.userApiForm.controls['id'].setValue(this.data.user.id);
      this.userApiForm.controls['username'].setValue(this.data.user.username);
    }
  }
  
  save(){
    if (!this.userApiForm.invalid) {
      this.loading = true;
      if(this.data.user !== undefined) {         
        this.editUser();
      } else{
        this.addUser();
      }
    }
  }

  addUser() {
    const userData = this.userApiForm.value;

    this.usersApiService.addUserApi(userData).subscribe((resp) => {
      this.loading = false;
      if (resp.status === 200) {
        this.notifyService.success('Usuario de aplicacion agregado exitosamente');
        this.dialogRef.close('ok');
      } else if (resp.status === 422) {
          var msgerror = '';
          $.each(resp.errors, function (indexInArray, valueOfElement) {
              msgerror += valueOfElement + '<br/>';
          });
          this.alertService.warning('Validacion', msgerror);
      } else {
          this.notifyService.error('Error guardando el usuario de aplicacion');
      }
    });
  }

  editUser() {
    const userData = {
      ...this.userApiForm.value,
      id: this.data.user.id
    };
    
    this.usersApiService.updateUserApi(userData).subscribe((resp) => {
      this.loading = false;
      if (resp.status === 200) {
        this.notifyService.success('Usuario de aplicacion editado exitosamente');
        this.dialogRef.close('ok');
      } else if (resp.status === 422) {
          var msgerror = '';
          $.each(resp.errors, function (indexInArray, valueOfElement) {
              msgerror += valueOfElement + '<br/>';
          });
          this.alertService.warning('Validacion', msgerror);
      } else {
          this.notifyService.error('Error editando el usuario de aplicacion');
      }
    });
  }

  get validation(): { [key: string]: AbstractControl } {
    return this.userApiForm.controls;
  }

}
