import { Component, OnInit, ViewChild  } from '@angular/core';
import { DatatablesService } from 'app/services/utils/datatables.service';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { CreateEditUsersApiComponent } from './modal/create-edit-users-api/create-edit-users-api.component';
import { UsersApiService } from 'app/services/users-api.service';
import { AlertService } from 'app/services/utils/alert.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-user-api',
  templateUrl: './user-api.component.html'
})
export class UserApiComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  loadingDt: boolean = false;

  users:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  columsTable = [
    { data: "id" },
    { data: "username" },
    { render: function (data: any, type: any, full: any) {
        return `<button class="btn btn-danger btn-link btn-just-icon mat-raised-button mat-button-base deleteTbl" mat-raised-button>
                <span class="mat-button-wrapper"><i class="material-icons">delete</i></span>
              </button>`;
      } 
    },
  ];

  constructor(
    private datatablesService: DatatablesService,
    private alertService: AlertService,
    private notifyService: NotifyService,
    private usersApiService: UsersApiService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.buildDataTeble();
  }

  buildDataTeble() {
    const that = this;
    this.dtOptions = this.datatablesService.configServerSide('/usersApi/all', this.columsTable);

    this.dtOptions.columnDefs = [
      { className: 'text-center', targets: [0,2] },
      { width: "10%", targets: [2] },
      { visible: false, targets: [ 0 ] }
    ];

    this.dtOptions.rowCallback = function(row: Node, data: any[] | Object, index: number){
      $('td .deleteTbl', row).on('click', () => {
        that.deleteUserApi(data);
      });
    }
  }

  addUserApi(){
    const dialogRef = this.dialog.open(CreateEditUsersApiComponent, {
      data: { title: 'Agregar' },
      width: '420px',
      disableClose: true
    });

    this.closeDialog(dialogRef);
  }

  deleteUserApi(dataUserApi:any){
    this.alertService.confirm(
      "Eliminar usuario de aplicacion", 
      "¿Esta seguro que desea eliminar el usuario de aplicacion "+dataUserApi.username+" ?"
    ).then((res) => {
      if (res) { 
        this.loadingDt = true;
        this.usersApiService.deleteUserApi({'id' : dataUserApi.id}).subscribe((resp) => {
          if (resp.status === 200) {
            this.loadingDt = false;
            this.notifyService.success('Usuario de aplicacion eliminado exitosamente');
            this.renderTable();
          } else if (resp.status === 422) {
              var msgerror = '';
              $.each(resp.errors, function (indexInArray, valueOfElement) {
                  msgerror += valueOfElement + '<br/>';
              });
              this.alertService.warning('Validacion', msgerror);
          } else {
              this.notifyService.error('Error eliminando al usuario de aplicacion');
          }
        });
      }
    });
  }

  closeDialog(dialogRef) {
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'ok') {
        this.renderTable();
      }
    });
  }

  renderTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

}
