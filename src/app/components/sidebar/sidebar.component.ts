import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/services/login.service';
import { UsersService } from 'app/services/users.service';
import { AlertService } from 'app/services/utils/alert.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTESADMIN: RouteInfo[] = [
    { path: '/home', title: 'Inicio',  icon: 'home', class: '' },
    { path: '/monitoring', title: 'Información SV',  icon:'swap_horiz', class: '' },
    { path: '/clients', title: 'Clientes',  icon:'folder_shared', class: '' },
    { path: '/aplications', title: 'Aplicaciones',  icon:'desktop_windows', class: '' },
    { path: '/users', title: 'Usuarios',  icon:'people', class: '' },
    { path: '/users-api', title: 'Usuarios Aplicacion',  icon:'recent_actors', class: '' },
    { path: '/logs-change', title: 'Logs Cambios',  icon:'format_list_bulleted', class: '' },
];

export const ROUTES: RouteInfo[] = [
    { path: '/home', title: 'Inicio',  icon: 'home', class: '' },
    { path: '/monitoring', title: 'Información SV',  icon:'swap_horiz', class: '' },
    { path: '/aplications', title: 'Clientes',  icon:'desktop_windows', class: '' },
    { path: '/logs-change', title: 'Usuarios',  icon:'format_list_bulleted', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(private loginService: LoginService, 
    private usersService: UsersService,
    private alertService: AlertService) { /** */ }

  ngOnInit() {
    const user = this.usersService.getUser();
    // console.log(user);
    this.menuItems = (user.admin == 1) ? ROUTESADMIN.filter(menuItem => menuItem) : ROUTES.filter(menuItem => menuItem) ;
  }

  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  }

  logOut() {
    this.alertService.confirm(
      "Cerrar Sesion", 
      "¿Esta seguro que desea finalizar sesion?"
    ).then((res) => {
      if (res) { 
        this.loginService.logout().subscribe(
          (response: any) => {
            localStorage.clear();
            location.reload();
          });
      }
    });
  }
}
