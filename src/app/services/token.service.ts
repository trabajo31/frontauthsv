import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

export interface AuthMe {
  status: number;
  data : any;
}

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private http: HttpClient) { }

  setToken(token) {
    localStorage.setItem('resourceToken', token);

    if (localStorage.getItem('resourceToken') !== null) {
      return true;
    } else {
      return false;
    }
  }

  getDataUserFromToken(param): Observable<AuthMe> {
    return this.http.post<AuthMe>( environment.urlApi + '/front/auth/me', param).pipe();
  }

  getToken(){
      return localStorage.getItem('resourceToken');
  }
  
  clearToken(){
    localStorage.clear();
  }
}
