import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { /** */ }
  
  confirm(
    title: string,
    text: string,
    confirmButtonText: string = 'Si',
    cancelButtonText: string = 'No'
  ) {
    return new Promise<any>(resolve => {
        Swal.fire({
            title: "<h3>"+title+"</h3>",
            text: text,
            confirmButtonText: '<i class="material-icons">check</i> '+confirmButtonText,
            cancelButtonText: '<i class="material-icons">close</i> '+cancelButtonText,
            showCancelButton: true,
            buttonsStyling: false,
            customClass: {
                confirmButton: "btn btn-sm btn-primary",
                cancelButton: "btn btn-sm btn-danger",
            }
        }).then(
            (result) => {
                if (result.value) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            }
        );
    });
  }

  warning(title: string, text: string) {
    Swal.fire({
        title: title,
        html: text,
        icon: 'warning',
        buttonsStyling: false,
        confirmButtonText: '<i class="material-icons">check</i> Aceptar',
        customClass: {
            confirmButton: "btn btn-sm btn-primary"
        }
    });
  }
}
