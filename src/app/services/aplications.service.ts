import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AplicationsService {

  constructor( private http: HttpClient ) { }

  getAplicationsAll(){
    return this.http.post<any>(environment.urlApi + '/front/service/list', {});
  }

  getServiceById(id){
    return this.http.post<any>(environment.urlApi + '/front/service/search', {id});
  }

  addAplications(data:any) {
    return this.http.post<any>(environment.urlApi + '/aplications/create', data);
  }

  updateAplications(data:any) {
    return this.http.post<any>(environment.urlApi + '/aplications/update', data);
  }

  changeDbAplications(data:any) {
    return this.http.post<any>(environment.urlApi + '/aplications/changeDb', data);
  }

  updateAplicationsDetails(data:any) {
    return this.http.post<any>(environment.urlApi + '/aplications/update/details', data);
  }

  changeStateAplications(data:any) {
    return this.http.post<any>(environment.urlApi + '/aplications/update/state', data);
  }
}
