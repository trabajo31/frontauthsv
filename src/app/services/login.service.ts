import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient){}

  login(params) {
    return this.http.post<any>( environment.urlApi + '/auth/login', params);
  }

  logout() {
    return this.http.post<any>( environment.urlApi + '/auth/logout', {});
  }
}
