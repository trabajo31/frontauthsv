import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor( private http: HttpClient ) { }
  
  getClientAll(){
    return this.http.post<any>(environment.urlApi + '/front/clients/getAll', {});
  }

  getClientList(){
    return this.http.post<any>(environment.urlApi + '/clients/list', {});
  }

  addClient(data:any) {
    return this.http.post<any>(environment.urlApi + '/clients/create', data);
  }

  updateClient(data:any) {
    return this.http.post<any>(environment.urlApi + '/front/clients/update', data);
  }

  deleteClient(data:any) {
    return this.http.post<any>(environment.urlApi + '/clients/delete', data);
  }
}
