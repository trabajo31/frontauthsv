import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersApiService {

  constructor( private http: HttpClient ) { }

  addUserApi(data:any) {
    return this.http.post<any>(environment.urlApi + '/usersApi/create', data);
  }

  updateUserApi(data:any) {
    return this.http.post<any>(environment.urlApi + '/usersApi/update', data);
  }

  deleteUserApi(data:any) {
    return this.http.post<any>(environment.urlApi + '/usersApi/delete', data);
  }
}
