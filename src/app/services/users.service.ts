import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DataUser } from 'app/commons/dataUser'; 

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor( private http: HttpClient ) { }

  getUserAll(){
    return this.http.post<any>(environment.urlApi +'/users/all', {});
  }

  getUserAllList(){
    return this.http.post<any>(environment.urlApi +'/users/list', {});
  }

  getRoles(){
    return this.http.post<any>(environment.urlApi +'/users/roles', {});
  }

  getUser(){
    return DataUser.user;
  }

  setUser(user:any){
    DataUser.user = user;
  }

  addUser(data:any) {
    return this.http.post<any>(environment.urlApi + '/users/create', data);
  }

  updateUser(data:any) {
    return this.http.post<any>(environment.urlApi + '/users/update', data);
  }

  deleteUser(data:any) {
    return this.http.post<any>(environment.urlApi + '/users/delete', data);
  }
}
